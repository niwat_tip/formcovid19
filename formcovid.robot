*** Settings ***
Library    Selenium2Library

*** Variables ***
${URL}                      https://forms.office.com/Pages/ResponsePage.aspx?id=ymGjQmCYm0OuHpeUrAVSsMPdkZtNedxBk3xxNap0sghUOFpEQTROSTRRRzZQMkhKMTZVVjIzSk5GQS4u
${DELAY}                    1

*** Keywords ***
Open Google Search Page
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${options}    add_argument    --headless
    Call Method    ${options}    add_argument    --disable-gpu
    Create Webdriver    Chrome    chrome_options=${options}
    Set Window Size    1920    1080
    Go To    ${URL}

Fill out on From Covid19

    input text              //*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[1]/div[2]/div[3]/div/div/div/input      บอล
    Click Element           //*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[3]/div/div[2]/div/label/input
    Click Element           //*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div/div[2]/div/label/input
    Click Element           //*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div/div[1]/div/label/input
    Set Selenium Speed        ${DELAY}
    Click Element           //*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[3]/div/button/div

Close Browser After Finish
    Set Selenium Speed        ${DELAY}
    close browser

*** Test Cases ***
Test Case: Search some keywords on Google search 
    Open Google Search Page
    Fill out on From Covid19
    Close Browser After Finish


    